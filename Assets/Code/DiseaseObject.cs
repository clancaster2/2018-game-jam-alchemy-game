﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Disease")]
public class DiseaseObject : ScriptableObject
{
    //Disease type and symptoms
    public string diseaseName;
    public List<PatientManager.symptoms> symptoms;

    public float lifeTimer;

    //Outlines the ingredients necessary for a successful cure
    public PatientManager.ingredients Cure1Ingredient1;
    public int Cure1Ingredient1Amount;
    public PatientManager.ingredients Cure1Ingredient2;
    public int Cure1Ingredient2Amount;
    public PatientManager.ingredients Cure1Ingredient3;
    public int Cure1Ingredient3Amount;
    public List<PatientManager.bodyParts> Cure1AdministrationLocations;
    public List<PatientManager.allergies> Cure1Allergies;
    public PatientManager.ingredients Cure2Ingredient1;
    public int Cure2Ingredient1Amount;
    public PatientManager.ingredients Cure2Ingredient2;
    public int Cure2Ingredient2Amount;
    public PatientManager.ingredients Cure2Ingredient3;
    public int Cure2Ingredient3Amount;
    public List<PatientManager.allergies> Cure2Allergies;
    public List<PatientManager.bodyParts> Cure2AdministrationLocations;
}

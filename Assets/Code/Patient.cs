﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Patient : MonoBehaviour
{
    private PatientManager manager;
    public string patientName;
	public Text nameText;
    public enum gender { male, female, };
    public gender patientGender;
	public Text genderText;
	public int patientAge;
	public Text ageText;
    public int patientID;
	public Text idText;
    public List<string> patientDetails = new List<string>();
	public Text detailsText;

    public DiseaseObject patientDisease;
	public Text diseaseText;
    public List<PatientManager.symptoms> patientSymptoms;
	public Text symptomsText;
    public List<PatientManager.allergies> patientAllergies;
	public Text allergiesText;
    public List<DiseaseObject> pastdiseases;
	public Text pastDiseasesText;
    public List<string> patientCrimes;
	public Text crimesText;
    public int crimeScore;

	public Text crimeDemo;

    public Timer timer;

    // Use this for initialization
    void Start()
    {
        //Stores a reference to the patient manager item
        manager = GameObject.Find("GameManager").GetComponent<PatientManager>();
        GenerateNewPatient();
        Temp.NewPatientGenerated += GenerateNewPatient;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void GenerateNewPatient()
    {
        ClearPatientData();
        //Selects patient name, gender, age and ID at random
        patientGender = (gender)Random.Range(0, System.Enum.GetValues(typeof(gender)).Length);
		genderText.text = patientGender.ToString();
        patientAge = Random.Range(12, 61);
		ageText.text = patientAge.ToString();
        patientName = manager.GenerateName(patientGender);
		nameText.text = patientName;
        patientID = manager.GenerateID();
        idText.text = patientID.ToString();
        //Asks the patient manager to randomly select a disease that the patient has
        patientDisease = manager.GenerateDisease();
        timer.SetTimer(patientDisease.lifeTimer);
        //diseaseText.text = patientDisease.ToString();
        foreach (PatientManager.symptoms symptom in patientDisease.symptoms)
        {
            patientSymptoms.Add(symptom);
            symptomsText.text = symptomsText.text + symptom + "\n";
        }
        //Randomly selects an amount of patient details, allergies, past diseases and crimes and asks the manager to fill them out at random
        int numOfDetails = Random.Range(1, 4);
        for (int i = 0; i <= numOfDetails; i++)
        {
            patientDetails.Add(RequestDetail());
        }
        foreach (string detail in patientDetails)
        {
            detailsText.text = detailsText.text + detail + "\n";
        }
        int numOfAllergies = Random.Range(0, 2);
        for (int i = 0; i <= numOfAllergies; i++)
        {
            patientAllergies.Add(RequestAllergy());
        }
        foreach (PatientManager.allergies allergy in patientAllergies)
        {
            allergiesText.text = allergiesText.text + allergy + "\n";
        }
        int numOfPastdiseases = Random.Range(0, 2);
        for (int i = 0; i <= numOfPastdiseases; i++)
        {
            //pastdiseases.Add(RequestDisease());
        }
        foreach (DiseaseObject disease in pastdiseases)
        {
            pastDiseasesText.text = pastDiseasesText.text + disease + "\n";
        }
        int numOfCrimes = Random.Range(1, 4);
        for (int i = 0; i < numOfCrimes; i++)
        {
            PatientManager.crimeStuct newCrime = RequestCrime();
            patientCrimes.Add(newCrime.crime);
            crimeScore += newCrime.crimeScore;
        }
        foreach (string crime in patientCrimes)
        {
            crimesText.text = crimesText.text + crime + "\n";
        }
        //crimeDemo.text = patientCrimes[0];
    }

    //Clears all patient data
    public void ClearPatientData()
    {
        patientDetails.Clear();
		detailsText.text = "";
        patientSymptoms.Clear();
		symptomsText.text = "";
		patientAllergies.Clear();
		allergiesText.text = "";
		pastdiseases.Clear();
        patientCrimes.Clear();
		crimesText.text = "";
		crimeScore = 0;
    }

    //Asks the manager to supply details
    private string RequestDetail()
    {
        string detail = manager.GenerateDetail();
        //Reselects any duplicated details
        //if (patientDetails.Contains(detail))
        //{
        //    detail = RequestDetail();
        //}
        return detail;
    }

    //Asks the manager to supply allergies
    private PatientManager.allergies RequestAllergy()
    {
        PatientManager.allergies allergy = manager.GenerateAllergies();
        //Reselects any duplicated allergies
        if (patientAllergies.Contains(allergy))
        {
            allergy = RequestAllergy();
        }
        return allergy;
    }

    //Asks the manager to supply diseases
    private DiseaseObject RequestDisease()
    {
        DiseaseObject disease = manager.GenerateDisease();
        //Reselects any duplicated diseases
        if (pastdiseases.Contains(disease))
        {
            disease = RequestDisease();
        }
        return disease;
    }

    //Asks the manager to supply crimes
    private PatientManager.crimeStuct RequestCrime()
    {
        PatientManager.crimeStuct crime = manager.GenerateCrime();
        //Reselects any duplicated crimes
        if (patientCrimes.Contains(crime.crime))
        {
            crime = RequestCrime();
        }
        return crime;
    }

    public string RequestOutcome()
    {
        string outcome = manager.GenerateOutcome(crimeScore);
        return outcome;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class PatientManager : MonoBehaviour
{
    //Variables for patient ID
    public static List<string> femaleNames;
    public static List<string> maleNames;
    public static List<string> lastNames;
    public static List<string> familyDetails;
    public static List<string> educationDetails;
    public static List<string> occupationDetails;
    public static List<string> alegianceDetails;
    public static List<string> interestsDetails;
    public static List<string> crimeDetails;
    public static List<string> outcomeDetails;
    public List<string> usedNames = new List<string>();
    public List<int> usedIDs = new List<int>();

    public string femaleFirstNamesFilePath;
    public string maleFirstNamesFilePath;
    public string lastNamesFilePath;
    public string educationFilePath;
    public string alegianceFilePath;
    public string familyFilePath;
    public string occupationFilePath;
    public string interestsFilePath;
    public string crimeFilePath;
    public string outcomeFilePath;


    //Variables for patient condition
    public List<DiseaseObject> diseases = new List<DiseaseObject>();

    public enum dataTypes
    {
        family,
        education,
        alegiance,
        occupation,
        interests,
    }
    public List<dataTypes> usedData;

    public enum symptoms
    {
        abdominalCramps,
        fever,
        headaches,
        coughing,
        chestPains,
        soreThroat,
        soreSkin,
        itchiness,
        fatigue,
        rash,
        vomiting,
        inflamedSkin,
        impairedVision,
    }

    public enum ingredients
    {
        giantsToe,
        frogTongue,
        phoenixWing,
        batEar,
        dragonsBreath,
        alcohol,
        ember,
        tundraIcycle,
        thunderFragment,
        trollsHair,
    }

    //I know this is a dupe of the ingredients, but I couldn't be bothered changing all the references in the scripts
    public enum allergies
    {
        giantsToe,
        frogTongue,
        phoenixWing,
        batEar,
        dragonsBreath,
        alcohol,
        ember,
        tundraIcycle,
        thunderFragment,
        trollsHair,
    }

    //Variables for administration location
    public enum bodyParts
    {
        Neck,
        Arm,
        Leg,
        Torso,
    }

    public enum cureResult
    {
        successful,
        wrongIngredients,
        wrongLocation,
        wrongAmount,
        allergic,
        outOfTime,
    }


    // Use this for initialization
    void Awake()
    {
        //Obtains and stores the lists of potential names, as well as all the data for other patient information.
        //string fdata = File.ReadAllText(Application.dataPath + "/TxtFiles/" + femaleFirstNamesFilePath);
        var fdata = Resources.Load<TextAsset>(femaleFirstNamesFilePath);//as TextAsset;
        //string mdata = File.ReadAllText(Application.dataPath + "/TxtFiles/" + maleFirstNamesFilePath);
        var mdata = Resources.Load<TextAsset>(maleFirstNamesFilePath);//as TextAsset;
        //string ldata = File.ReadAllText(Application.dataPath + "/TxtFiles/" + lastNamesFilePath);
        var ldata = Resources.Load<TextAsset>(lastNamesFilePath);//as TextAsset;
        //string familyData = File.ReadAllText(Application.dataPath + "/TxtFiles/" + familyFilePath);
        var familyData = Resources.Load<TextAsset>(familyFilePath);
        //string alegianceData = File.ReadAllText(Application.dataPath + "/TxtFiles/" + alegianceFilePath);
        var alegianceData = Resources.Load<TextAsset>(alegianceFilePath);
        //string occupationData = File.ReadAllText(Application.dataPath + "/TxtFiles/" + occupationFilePath);
        var occupationData = Resources.Load<TextAsset>(occupationFilePath);
        //string educationData = File.ReadAllText(Application.dataPath + "/TxtFiles/" + educationFilePath);
        var educationData = Resources.Load<TextAsset>(educationFilePath);
        //string interestsData = File.ReadAllText(Application.dataPath + "/TxtFiles/" + interestsFilePath);
        var interestsData = Resources.Load<TextAsset>(interestsFilePath);
        //string crimeData = File.ReadAllText(Application.dataPath + "/TxtFiles/" + crimeFilePath);
        var crimeData = Resources.Load<TextAsset>(crimeFilePath);
        //string outcomeData = File.ReadAllText(Application.dataPath + "/TxtFiles/" + outcomeFilePath);
        var outcomeData = Resources.Load<TextAsset>(outcomeFilePath);
        femaleNames = fdata.text.Split('\n').ToList();
        maleNames = mdata.text.Split('\n').ToList();
        lastNames = ldata.text.Split('\n').ToList();
        familyDetails = familyData.text.Split('\n').ToList();
        educationDetails = educationData.text.Split('\n').ToList();
        occupationDetails = occupationData.text.Split('\n').ToList();
        alegianceDetails = alegianceData.text.Split('\n').ToList();
        interestsDetails = interestsData.text.Split('\n').ToList();
        crimeDetails = crimeData.text.Split('\n').ToList();
        outcomeDetails = outcomeData.text.Split('\n').ToList();
    }

    void Start()
    {
        Temp.NewPatientGenerated += ClearUsedData;
    }

    // Update is called once per frame
    void Update()
    {

    }

    //Generates a random and unique name from 3 separate lists of names
    public string GenerateName(Patient.gender gender)
    {
        string firstName = GenerateFirstName(gender);
        string lastName = GenerateLastName();
        string fullname = firstName + " " + lastName;
        if (usedNames.Contains(fullname))
        {
            Debug.Log("name has been used");
            fullname = GenerateName(gender);
        }
        else
        {
            usedNames.Add(fullname);
        }
        return fullname;
    }

    private string GenerateFirstName(Patient.gender gender)
    {
        //Randomly generates a int to specify which line to read
        if (gender == Patient.gender.male)
        {
            int lineNumber = Random.Range(1, 299);
            return maleNames[lineNumber];
        }
        else if(gender == Patient.gender.female)
        {
            int lineNumber = Random.Range(1, 89);
            return femaleNames[lineNumber];
        }
        else
        {
            return null;
        }
    }

    public string GenerateLastName()
    {
        //Randomly generates a int to specify which line to read
        int lineNumber = Random.Range(1, 120);
        return lastNames[lineNumber];
    }

    //Generates a unique 8 digit number for the patients ID
    public int GenerateID()
    {
        int ID = Random.Range(10000000, 100000000);
        if (usedIDs.Contains(ID))
        {
            Debug.Log("ID has been used");
            ID = GenerateID();
        }
        else
        {
            usedIDs.Add(ID);
        }
        return ID;
    }

    //Generates a few personal details for the patient
    public string GenerateDetail()
    {
        dataTypes thisDetail = GenerateDetailType();
        string data = "blank";
        int lineNumber;
        switch (thisDetail)
        {
            case dataTypes.alegiance:
                lineNumber = Random.Range(1, 25);
                data = alegianceDetails[lineNumber];
                break;
            case dataTypes.education:
                lineNumber = Random.Range(1, 9);
                data = educationDetails[lineNumber];
                break;
            case dataTypes.family:
                lineNumber = Random.Range(1, 16);
                data = familyDetails[lineNumber];
                break;
            case dataTypes.interests:
                lineNumber = Random.Range(1, 25);
                data = interestsDetails[lineNumber];
                break;
            case dataTypes.occupation:
                lineNumber = Random.Range(1, 24);
                data = occupationDetails[lineNumber];
                break;
        }
        return data;
    }

    private dataTypes GenerateDetailType()
    {
        dataTypes detail = (dataTypes)Random.Range(0, System.Enum.GetValues(typeof(dataTypes)).Length);
        if (usedData.Contains(detail))
        {
            detail = GenerateDetailType();
        }
        usedData.Add(detail);
        return detail;
    }

    //Resets some of the used data checks for new patients
    //Should be launched before generating a new patient by event or something
    private void ClearUsedData()
    {
        usedData.Clear();
    }

    //Generates a random disease
    public DiseaseObject GenerateDisease()
    {
        DiseaseObject thisDisease = diseases[Random.Range(0, diseases.Count)];
        return thisDisease;
    }

    //Generates a random allergy
    public allergies GenerateAllergies()
    {
        allergies allergy = (allergies)Random.Range(0, System.Enum.GetValues(typeof(allergies)).Length);
        return allergy;
    }

    //Generates a random crime
    public crimeStuct GenerateCrime()
    {
        int lineNumber = Random.Range(1, 71);
        string crime = crimeDetails[lineNumber];
        crimeStuct thisCrime = new crimeStuct();
        thisCrime.crime = crime;
        thisCrime.crimeScore = lineNumber;
        return thisCrime;
    }

    public struct crimeStuct
    {
        public string crime;
        public int crimeScore;
    };

    public string GenerateOutcome(int crimeScore)
    {
        string outcome;
        int lineNumber;
        if (crimeScore < 50)
        {
            lineNumber = Random.Range(0, 9);
            outcome = outcomeDetails[lineNumber]; 
        }
        else
        {
            lineNumber = Random.Range(10, 19);
            outcome = outcomeDetails[lineNumber];
        }
        return outcome;
    }
}

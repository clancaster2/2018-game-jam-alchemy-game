﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConcoctionManager : MonoBehaviour
{
	//Variables for players chosen ingredients
	public PatientManager.ingredients ingredient1;
	public int ingredient1Amount;
	public bool ingredient1Assigned;
	public PatientManager.ingredients ingredient2;
	public int ingredient2Amount;
	public bool ingredient2Assigned;
	public PatientManager.ingredients ingredient3;
	public int ingredient3Amount;
	public bool ingredient3Assigned;

	public PatientManager.bodyParts adminstrationLocation;

	private Patient currentPatient;
	private Temp tempScript;
    public Transform PanelImage;

	// Use this for initialization
	void Start()
	{
		ingredient1Assigned = false;
		ingredient2Assigned = false;
		ingredient3Assigned = false;
		currentPatient = GameObject.Find("Patient").GetComponent<Patient>();
		tempScript = GetComponent<Temp>();
		Temp.NewPatientGenerated += ResetIngredients;
	}

	// Update is called once per frame
	void Update()
	{

	}

	public void AddIngredient(PatientManager.ingredients ingredientType)
	{
		//Checks if the player has chosen their first ingredient yet
		if (!ingredient1Assigned)
		{
			//Assigns the first ingredient and increases its amount
			ingredient1Assigned = true;
			ingredient1 = ingredientType;
			ingredient1Amount++;
			return;
		}
		//If the first ingredient has been previously assigned, is this ingredient of the same type??
		else if (ingredient1 == ingredientType)
		{
			//increases amount by one
			ingredient1Amount++;
			return;
		}
		//Repeats the above process for the three ingredients
		if (!ingredient2Assigned)
		{
			ingredient2Assigned = true;
			ingredient2 = ingredientType;
			ingredient2Amount++;
			return;
		}
		else if (ingredient2 == ingredientType)
		{
			ingredient2Amount++;
			return;
		}
		if (!ingredient3Assigned)
		{
			ingredient3Assigned = true;
			ingredient3 = ingredientType;
			Debug.Log("Add to 3");
			ingredient3Amount++;
			return;
		}
		else if (ingredient3 == ingredientType)
		{
			Debug.Log("Add more to 3");
			ingredient3Amount++;
			return;
		}
	}

	//At the end of a patient, empty/unassign the ingredients
	public void ResetIngredients()
	{
        
		//Unassigns all the ingredients 
		ingredient1Assigned = false;
		ingredient2Assigned = false;
		ingredient3Assigned = false;
		//Sets all the ingredient amounts back to 0
		ingredient1Amount = 0;
		ingredient2Amount = 0;
		ingredient3Amount = 0;
	}

	public PatientManager.cureResult CompareCureOne()
	{
        //Checks if first ingredient is successful
        if (ingredient1 == currentPatient.patientDisease.Cure1Ingredient1)
		{
			if (ingredient1Amount == currentPatient.patientDisease.Cure1Ingredient1Amount)
			{
				//Ingredient one successful  
			}
			else
			{
				Debug.Log("1 - 1");
				return PatientManager.cureResult.wrongAmount;
			}
		}
		else if (ingredient2 == currentPatient.patientDisease.Cure1Ingredient1)
		{
			if (ingredient2Amount == currentPatient.patientDisease.Cure1Ingredient1Amount)
			{
				//Ingredient one successful  
			}
			else
			{
				Debug.Log("2 - 1");
				return PatientManager.cureResult.wrongAmount;
			}
		}
		else if (ingredient3 == currentPatient.patientDisease.Cure1Ingredient1)
		{
			if (ingredient3Amount == currentPatient.patientDisease.Cure1Ingredient1Amount)
			{
				//Ingredient one successful  
			}
			else
			{
				Debug.Log("3 - 1");
				return PatientManager.cureResult.wrongAmount;
			}
		}
		else
		{
			//Ingredient 1 completely fucked, pack it up boys, we're done here
			return PatientManager.cureResult.wrongIngredients;
		}
		//Checks if second ingredient is successful
		if (ingredient1 == currentPatient.patientDisease.Cure1Ingredient2)
		{
			if (ingredient1Amount == currentPatient.patientDisease.Cure1Ingredient2Amount)
			{
				//Ingredient two successful  
			}
			else
			{
				Debug.Log("1 - 2");
				return PatientManager.cureResult.wrongAmount;
			}
		}
		else if (ingredient2 == currentPatient.patientDisease.Cure1Ingredient2)
		{
			if (ingredient2Amount == currentPatient.patientDisease.Cure1Ingredient2Amount)
			{
				//Ingredient two successful  
			}
			else
			{
				Debug.Log("2 - 2");
				return PatientManager.cureResult.wrongAmount;
			}
		}
		else if (ingredient3 == currentPatient.patientDisease.Cure1Ingredient2)
		{
			if (ingredient3Amount == currentPatient.patientDisease.Cure1Ingredient2Amount)
			{
				//Ingredient two successful  
			}
			else
			{
				Debug.Log("3 - 2");
				return PatientManager.cureResult.wrongAmount;
			}
		}
		else
		{
			//Ingredient 2 completely fucked, pack it up boys, we're done here
			return PatientManager.cureResult.wrongIngredients;
		}
		//Checks if third ingredient is successful
		if (ingredient1 == currentPatient.patientDisease.Cure1Ingredient3)
		{
			if (ingredient1Amount == currentPatient.patientDisease.Cure1Ingredient3Amount)
			{
				//Ingredient three successful  
			}
			else
			{
				Debug.Log("1 - 3");
				return PatientManager.cureResult.wrongAmount;
			}
		}
		else if (ingredient2 == currentPatient.patientDisease.Cure1Ingredient3)
		{
			if (ingredient2Amount == currentPatient.patientDisease.Cure1Ingredient3Amount)
			{
				//Ingredient three successful  
			}
			else
			{
				Debug.Log("2 - 3");
				return PatientManager.cureResult.wrongAmount;
			}
		}
		else if (ingredient3 == currentPatient.patientDisease.Cure1Ingredient3)
		{
			if (ingredient3Amount == currentPatient.patientDisease.Cure1Ingredient3Amount)
			{
				//Ingredient three successful  
			}
			else
			{
				Debug.Log("3 - 3");
				Debug.Log(ingredient3Amount);
				Debug.Log(currentPatient.patientDisease.Cure1Ingredient3Amount);
				return PatientManager.cureResult.wrongAmount;
			}
		}
		else
		{
			//Ingredient 1 completely fucked, pack it up boys, we're done here
			return PatientManager.cureResult.wrongIngredients;
		}
		//Checks if the patient has one of the cures matched allergies
		foreach (PatientManager.allergies allergy in currentPatient.patientDisease.Cure1Allergies)
		{
			foreach (PatientManager.allergies patientAllergy in currentPatient.patientAllergies)
			{
				if (patientAllergy == allergy)
				{
					//One of the patients allergies matches with the cures bad allergies, so therefore the cure fails
					Debug.Log("Patient is allergic");
					return PatientManager.cureResult.allergic;
				}
			}
		}
		//If all the ingredients are correct, check if the cure has been administered in the right location
		foreach (PatientManager.bodyParts locations in currentPatient.patientDisease.Cure1AdministrationLocations)
		{
			if (adminstrationLocation == locations)
			{
				Debug.Log("Cure Found");
				return PatientManager.cureResult.successful;
			}
		}
		return PatientManager.cureResult.wrongLocation;
	}



	public PatientManager.cureResult CompareCureTwo()
	{
		//Checks if first ingredient is successful
		if (ingredient1 == currentPatient.patientDisease.Cure2Ingredient1)
		{
			if (ingredient1Amount == currentPatient.patientDisease.Cure2Ingredient1Amount)
			{
				//Ingredient one successful  
			}
			else
			{
				return PatientManager.cureResult.wrongAmount;
			}
		}
		else if (ingredient2 == currentPatient.patientDisease.Cure2Ingredient1)
		{
			if (ingredient2Amount == currentPatient.patientDisease.Cure2Ingredient1Amount)
			{
				//Ingredient one successful  
			}
			else
			{
				return PatientManager.cureResult.wrongAmount;
			}
		}
		else if (ingredient3 == currentPatient.patientDisease.Cure2Ingredient1)
		{
			if (ingredient3Amount == currentPatient.patientDisease.Cure2Ingredient1Amount)
			{
				//Ingredient one successful  
			}
			else
			{
				return PatientManager.cureResult.wrongAmount;
			}
		}
		else
		{
			//Ingredient 1 completely fucked, pack it up boys, we're done here
			return PatientManager.cureResult.wrongIngredients;
		}
		//Checks if second ingredient is successful
		if (ingredient1 == currentPatient.patientDisease.Cure2Ingredient2)
		{
			if (ingredient1Amount == currentPatient.patientDisease.Cure2Ingredient2Amount)
			{
				//Ingredient two successful  
			}
			else
			{
				return PatientManager.cureResult.wrongAmount;
			}
		}
		else if (ingredient2 == currentPatient.patientDisease.Cure2Ingredient2)
		{
			if (ingredient2Amount == currentPatient.patientDisease.Cure2Ingredient2Amount)
			{
				//Ingredient two successful  
			}
			else
			{
				return PatientManager.cureResult.wrongAmount;
			}
		}
		else if (ingredient3 == currentPatient.patientDisease.Cure2Ingredient2)
		{
			if (ingredient3Amount == currentPatient.patientDisease.Cure2Ingredient2Amount)
			{
				//Ingredient two successful  
			}
			else
			{
				return PatientManager.cureResult.wrongAmount;
			}
		}
		else
		{
			//Ingredient 2 completely fucked, pack it up boys, we're done here
			return PatientManager.cureResult.wrongIngredients;
		}
		//Checks if third ingredient is successful
		if (ingredient1 == currentPatient.patientDisease.Cure2Ingredient3)
		{
			if (ingredient1Amount == currentPatient.patientDisease.Cure2Ingredient3Amount)
			{
				//Ingredient three successful  
			}
			else
			{
				return PatientManager.cureResult.wrongAmount;
			}
		}
		else if (ingredient2 == currentPatient.patientDisease.Cure2Ingredient3)
		{
			if (ingredient2Amount == currentPatient.patientDisease.Cure2Ingredient3Amount)
			{
				//Ingredient three successful  
			}
			else
			{
				return PatientManager.cureResult.wrongAmount;
			}
		}
		else if (ingredient3 == currentPatient.patientDisease.Cure2Ingredient3)
		{
			if (ingredient3Amount == currentPatient.patientDisease.Cure2Ingredient3Amount)
			{
				//Ingredient three successful  
			}
			else
			{
				return PatientManager.cureResult.wrongAmount;
			}
		}
		else
		{
			//Ingredient 1 completely fucked, pack it up boys, we're done here
			return PatientManager.cureResult.wrongIngredients;
		}
		//Checks if the patient has one of the cures matched allergies
		foreach (PatientManager.allergies allergy in currentPatient.patientDisease.Cure2Allergies)
		{
			foreach (PatientManager.allergies patientAllergy in currentPatient.patientAllergies)
			{
				if (patientAllergy == allergy)
				{
					//One of the patients allergies matches with the cures bad allergies, so therefore the cure fails
					Debug.Log("Patient is allergic");
					return PatientManager.cureResult.allergic;
				}
			}
		}
		//If all the ingredients are correct, check if the cure has been administered in the right location
		foreach (PatientManager.bodyParts locations in currentPatient.patientDisease.Cure2AdministrationLocations)
		{
			if (adminstrationLocation == locations)
			{
				Debug.Log("Cure Found");
				return PatientManager.cureResult.successful;
			}
		}
		return PatientManager.cureResult.wrongLocation;
	}

	public void AdministerPotionNeck()
	{
		adminstrationLocation = PatientManager.bodyParts.Neck;
		tempScript.CheckCure();
        ClearIngredientsList();
    }

	public void AdministerPotionTorso()
	{
		adminstrationLocation = PatientManager.bodyParts.Torso;
		tempScript.CheckCure();
        ClearIngredientsList();
    }

	public void AdministerPotionLeg()
	{
		adminstrationLocation = PatientManager.bodyParts.Leg;
		tempScript.CheckCure();
        ClearIngredientsList();
    }

	public void AdministerPotionArm()
	{
		adminstrationLocation = PatientManager.bodyParts.Arm;
		tempScript.CheckCure();
        ClearIngredientsList();
    }

    public void ClearIngredientsList()
    {
        //Removes ingredients list from panel
        for(int i = 0; i < PanelImage.childCount; i++)
        {
            Destroy(PanelImage.GetChild(i).gameObject);
        }
    }
}

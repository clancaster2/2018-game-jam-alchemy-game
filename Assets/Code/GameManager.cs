﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public int patientsSaved;
    public int patientsKilled;
    public int notority;
    public int morality;
    public int notorityChange;
    public int moralityChange;
    public bool gameOver;
    public Patient currentPatient;
    public GameObject ResultScreen;
    public Text outcomeText;
    public GameObject endGameUI;

	public Image moralityBar;
    public Image notorityBar;

    // Use this for initialization
    void Start()
    {
        outcomeText.supportRichText = true;
        Temp.SavedPatient += IncreaseSaved;
        Temp.KilledPatient += IncreaseKilled;
        Timer.OutOfTime += IncreaseKilled;
		currentPatient = GameObject.Find("Patient").GetComponent<Patient>();
		morality = 50;
        notority = 0;
        endGameUI.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (notority < 0)
        {
            notority = 0;
        }
        if (!gameOver)
        {
            if (notority >= 100)
            {
                gameOver = true;
                Debug.Log("Player caught");
                //Loss state here 
                endGameUI.SetActive(true);
                endGameUI.GetComponent<Text>().text = "You were caught, game over";
            }
            if (morality < 0)
            {
                gameOver = true;
                Debug.Log("Fuck morals amirite");
                //And here
                endGameUI.SetActive(true);
                endGameUI.GetComponent<Text>().text = "You went insane, game over";
            }
        }
		if (morality > 100)
		{
			morality = 100;
            //Do a win state here 
            gameOver = true;
            endGameUI.SetActive(true);
            endGameUI.GetComponent<Text>().text = "You did good, game over";
        }
		float moralBar = (float)morality / 100.0f;
        float notorityBarFill = (float)notority / 100.0f;
		moralityBar.fillAmount = moralBar;
        notorityBar.fillAmount = notorityBarFill;
	}

    public void IncreaseSaved(PatientManager.cureResult result, int moralScore)
    {
        patientsSaved++;
        //Lowers the players notority when they save a patient
        if (notority > 0)
        {
            notorityChange = 5;
            notority -= notorityChange;
        }
        EvaluatePatient(moralScore, true);
    }

    public void IncreaseKilled(PatientManager.cureResult result, int moralScore)
    {
        Debug.Log(result);
        patientsKilled++;
        //Raises the players notority based on how the patient died
        if (result == PatientManager.cureResult.allergic)
        {
            notorityChange = 15;
            notority += notorityChange;
        }
        if (result == PatientManager.cureResult.wrongIngredients)
        {
            notorityChange = 20;
            notority += notorityChange;
        }
        if (result == PatientManager.cureResult.wrongAmount)
        {
            notorityChange = 10;
            notority += notorityChange;
        }
        if (result == PatientManager.cureResult.wrongLocation)
        {
            notorityChange = 15;
            notority += notorityChange;
        }
        if (result == PatientManager.cureResult.outOfTime)
        {
            ResultScreen.SetActive(true);
            notorityChange = 20;
            notority += notorityChange;
        }
        EvaluatePatient(moralScore, false);
    }

    public void EvaluatePatient(int score, bool saved)
    {
        if (saved)
        {
            string outcome = currentPatient.RequestOutcome();
            if (score > 50)
            {
                moralityChange = -(score / 6);
                morality += moralityChange;
            }
            else
            {
                moralityChange = score / 4;
                morality += moralityChange;
            }
            outcomeText.text = currentPatient.patientName + outcome + "\n" + "You lost <color=red>" + notorityChange + "</color> notoriety, and morality changed by <color=blue>" + moralityChange + "</color>";
            Debug.Log(outcome);
        }
        else if (!saved)
        {
            if (score > 50)
            {
                moralityChange = score / 6;
                morality += moralityChange;
                outcomeText.text = "They were pretty bad." + "\n" + "You gained <color=red>" + notorityChange + "</color> notoriety, and morality changed by <color=blue>" + moralityChange + "</color>";
            }
            else
            {
                moralityChange = -(score / 2);
                morality += moralityChange;
                outcomeText.text = "They weren't that bad, did they deserve to die?" + "\n" + "You gained <color=red>" + notorityChange + "</color> notoriety, and morality changed by <color=blue>" + moralityChange + "</color>";
            }
            
        }
    }
}

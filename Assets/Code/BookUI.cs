﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BookUI : MonoBehaviour
{
	private AudioSource BookOpening;
	public GameObject BookGameObject;
	public GameObject BookButton;
	public GameObject BackButton;
    public Sprite[] Pages;
    public Image PageSprite;
    private int PageNumber;
    public AudioClip PageFlip;
    public AudioClip BookPickup;

	// Use this for initialization
	void Start()
	{
		BookOpening = GetComponent<AudioSource>();
        PageNumber = 0;
	}

	// Update is called once per frame
	void Update()
	{

	}

	public void OpenBook()
	{
		if (BookGameObject != null)
		{
			BookGameObject.SetActive(true);
			
			BackButton.SetActive(true);
		}

        if(BookButton != null)
        {
            BookButton.SetActive(false);
        }
        BookOpening.clip = BookPickup;
        BookOpening.Play();
	}

	public void CloseBook()
	{
		BookOpening.Play();

		if (BookGameObject != null)
        { 
			BackButton.SetActive(false);
			BookGameObject.SetActive(false);
		}
        if (BookButton != null)
        {
            BookButton.SetActive(true);
        }

        BookOpening.clip = BookPickup;
        BookOpening.Play();

        PageNumber = 0;
    }

    public void PageChange()
    {
        if (PageNumber < Pages.Length - 1)
        {
            PageNumber += 1;
            PageSprite.sprite = Pages[PageNumber];
            BookOpening.clip = PageFlip;
            BookOpening.Play();
        }
    }

    public void PageBack()
    {
        if (PageNumber > 0)
        {
            PageNumber -= 1;
            PageSprite.sprite = Pages[PageNumber];
            BookOpening.clip = PageFlip;
            BookOpening.Play();
        }
    }
}

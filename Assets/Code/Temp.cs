﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Temp : MonoBehaviour
{
    public ConcoctionManager CureManager;
    public delegate void NewPatient();
    public static event NewPatient NewPatientGenerated;
    public delegate void PatientResult(PatientManager.cureResult result, int moralScore);
    public static event PatientResult KilledPatient;
    public static event PatientResult SavedPatient;

    public GameObject ResultScreen;

    private Patient currentPatient;

    public Timer timer;

    // Use this for initialization
    void Start()
    {
        CureManager = GameObject.Find("GameManager").GetComponent<ConcoctionManager>();
        currentPatient = GameObject.Find("Patient").GetComponent<Patient>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void AddOne()
    {
        CureManager.AddIngredient(PatientManager.ingredients.giantsToe);
    }

    public void AddTwo()
    {
        CureManager.AddIngredient(PatientManager.ingredients.frogTongue);
    }

    public void AddThree()
    {
        CureManager.AddIngredient(PatientManager.ingredients.phoenixWing);
    }

    public void CheckCure()
    {
        PatientManager.cureResult working = CureManager.CompareCureOne();
        if (working == PatientManager.cureResult.successful)
        {
            if (SavedPatient != null) SavedPatient(working, currentPatient.crimeScore);
        }
        else
        {
            PatientManager.cureResult working2 = CureManager.CompareCureTwo();
            if (working2 == PatientManager.cureResult.successful)
            {
                if (SavedPatient != null) SavedPatient(working2, currentPatient.crimeScore);
            }
            else
            {
                if (working == PatientManager.cureResult.wrongIngredients)
                {
                    if (KilledPatient != null) KilledPatient(working2, currentPatient.crimeScore);
                }
                else
                {
                    if (KilledPatient != null) KilledPatient(working, currentPatient.crimeScore);
                }
            }
        }
        ResultScreen.SetActive(true);
        timer.timerActive = false;
    }

    public void GetNewPatient()
    {
        ResultScreen.SetActive(false);
        if (!GameObject.Find("GameManager").GetComponent<GameManager>().gameOver)
        {
            Debug.Log("Generating new patient");
            if (NewPatientGenerated != null) NewPatientGenerated();
        }
    }
}

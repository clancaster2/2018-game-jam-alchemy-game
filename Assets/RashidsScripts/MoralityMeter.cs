﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoralityMeter : MonoBehaviour {

    public Image moralityMeterBar;
    public Image investigationMeterBar;

    public Text moralityText;
    public Text investigationText;


	// Use this for initialization
	void Start ()
    {


		
	}

    void LowerMoralityMeter(float amount)
    {
        moralityMeterBar.fillAmount -= amount;
        moralityText.text = (moralityMeterBar.fillAmount * 100).ToString("##") + " % ";
    }

    void LowerInvestigationMeter(float amount)
    {
        investigationMeterBar.fillAmount -= amount;
        investigationText.text = (investigationMeterBar.fillAmount * 100).ToString("##") + " % ";
    }

    // Update is called once per frame
    void FixedUpdate () {

        LowerMoralityMeter(0.001f);
        LowerInvestigationMeter(0.001f);

        //InvestigationMetrBar.fillAmount -= 1f;

        //InvestigationText.text = moralityMeterBar.fillAmount.ToString("##") + " % ";

    }

    
}

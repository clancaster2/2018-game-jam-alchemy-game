﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VialScript : MonoBehaviour {

    private Selector selector;
    public Vector3 startPos;
	public PatientManager.ingredients thisIngredient;
    public GameObject UISprite;
    public AudioSource PotionAudio;
    public AudioClip PotionPickUp;
    public AudioClip PotionPour;
    public string HoverOverText;
    public Sprite Icon;

	// Use this for initialization
	void Start () {

        startPos = transform.position;
        selector = GameObject.Find("Selector").GetComponent<Selector>();
        PotionAudio = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter(Collision collision)
    { 
        if (collision.gameObject.tag == "Mortar")
        {
            selector.vialTouching = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Mortar")
        {
            selector.vialTouching = false;
        }
    }
}

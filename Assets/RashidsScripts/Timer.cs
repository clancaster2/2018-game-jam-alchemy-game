﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

    public Text time;

    public float timer;
    public bool timerActive;

    public delegate void TimeOut(PatientManager.cureResult result, int moralScore);
    public static event TimeOut OutOfTime;

	// Use this for initialization
	void Start ()
    { 

        //Set patient time Variable here
        //timer = 60f;



    }

    public void SetTimer(float time)
    {
        Debug.Log("Set timer");
        timer = time;
        timerActive = true;
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (timerActive)
        {
            timer -= Time.deltaTime;

            time.text = "Time Left: " + timer.ToString("##");

            if (timer <= 0f)
            {
                time.text = "Time Left: 0";
                timer = 0f;
                //Launch dat sweet sweet fail state
                if (OutOfTime != null) OutOfTime(PatientManager.cureResult.outOfTime, GameObject.Find("Patient").GetComponent<Patient>().crimeScore);
                timerActive = false;
            }
        }
	}
}

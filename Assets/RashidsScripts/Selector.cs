﻿using UnityEngine;
using UnityEngine.UI;

public class Selector : MonoBehaviour
{

    public bool _mouseState;

    private GameObject vial;
    public GameObject mortar;
    public GameObject shelf;
    public GameObject book;
    public BookUI BookScript;

    public Vector3 screenSpace;
    public Vector3 offset;
    public GameObject UIPanel;

    public bool vialTouching;

	public ConcoctionManager CM;
    public GameObject HoverOverImage;
    public Image IconImage;
    public GameObject bookHoverOver;

    public Text HoverOverText;

    // Use this for initialization
    void Start()
    {
        BookScript = GameObject.Find("Canvas").GetComponent<BookUI>();


    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            _mouseState = false;

            if (vial != null)
            {
                vial.transform.position = vial.GetComponent<VialScript>().startPos;
                vial.GetComponent<VialScript>().PotionAudio.clip = vial.GetComponent<VialScript>().PotionPour;
                vial.GetComponent<VialScript>().PotionAudio.Play();
                //vial = null;
            }


            if (vialTouching)
            {
                var SpawnedSprite = Instantiate(vial.GetComponent<VialScript>().UISprite, UIPanel.transform);
                //SpawnedSprite.transform.parent = UIPanel.transform;
                SpawnedSprite.transform.SetParent(UIPanel.transform, false);
                Debug.Log("Boom!!");
				CM.AddIngredient(vial.GetComponent<VialScript>().thisIngredient);
				vialTouching = false;
                vial = null;

			}
            else
            {

            }

        }
    }

    public void FixedUpdate()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray))
        {
            RaycastHit hit;


            if (Physics.Raycast(ray, out hit, 100.0f))
            {
                if (hit.collider.gameObject.tag == "Vial" )
                {
                    vial = hit.collider.gameObject;
                    if (!_mouseState)
                    {
                        HoverOverImage.SetActive(true);
                        Vector3 Pos = Camera.main.WorldToScreenPoint(hit.point);
                        HoverOverImage.transform.position = new Vector3(Pos.x, Pos.y + 80, Pos.z);
                        //HoverOverText.text = vial.GetComponent<VialScript>().HoverOverText;
                        IconImage.sprite = vial.GetComponent<VialScript>().Icon;
                    }

                    if ((Input.GetMouseButtonDown(0)))
                    {
                        HoverOverImage.SetActive(false);
                        bookHoverOver.SetActive(false);
                        vial.GetComponent<VialScript>().PotionAudio.clip = vial.GetComponent<VialScript>().PotionPickUp;
                        vial.GetComponent<VialScript>().PotionAudio.Play();

                        Debug.Log("Vial Held");
                        Debug.DrawLine(ray.origin, ray.direction, Color.red);

                        _mouseState = true;
                        screenSpace = Camera.main.WorldToScreenPoint(vial.transform.position);
                        offset = vial.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenSpace.z));
                    }

                    
                }

               if(hit.collider.gameObject == book)
                {
                    bookHoverOver.SetActive(true);

                    if ((Input.GetMouseButtonDown(0)))
                    {
                        BookScript.OpenBook();
                    }
                }

                if (hit.collider.gameObject == mortar)
                {
                    //Debug.Log("Hit Mortar");
                    Debug.DrawLine(ray.origin, ray.direction, Color.red);
                }
                if (hit.collider.gameObject == shelf)
                {
                    //Debug.Log("Hit Shelf");
                    Debug.DrawLine(ray.origin, ray.direction, Color.red);
                }

            }
            else
            {
                Debug.Log("Did not Hit");
            }
        }
        else
        {
            print("hello");
            HoverOverImage.SetActive(false);
            bookHoverOver.SetActive(false);
        }

        if (_mouseState)
        {
            //keep track of the mouse position
            var curScreenSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenSpace.z);

            //convert the screen mouse position to world point and adjust with offset
            var curPosition = Camera.main.ScreenToWorldPoint(curScreenSpace) + offset;

            //update the position of the object in the world
            vial.transform.position = curPosition;
        }


    }

}